import particles from 'react-tsparticles';
import configpar from './config/Configpar.js'


const ParticlesBackground = () => {
    return(
        <particles params={configpar}>

        </particles>
    )
}

export default ParticlesBackground