import React,{useState} from "react";
import Axios from "axios"; 
import './App.css';
import { GiButterfly} from 'react-icons/gi'
import { VscSearch } from 'react-icons/vsc';
import {  FiDelete } from 'react-icons/fi';
import {  AiOutlineArrowLeft,AiOutlineArrowRight } from 'react-icons/ai';
//import ParticlesBackground from './components/ParticalBg.js';

const shortId = require('shortid'); // Its  creating unique ID

function App() {
  const [state, setState]= useState(false); //state and setState using for using for copy the link and using useState for adding styling
  const [url , seturl] = useState("");  // using useState for setting url through event handler
  const [listUrl , geturl] = useState([]); // for storing db response 
  

  // Function for generating short url

  const submiturl = () => {
      const sendLink = {
        furl: url,
        ukey: shortId.generate() // Generating unique ID
      }
      Axios.post('http://localhost:3001/saveurl', sendLink).then(() => {
        console.log("server working");
      })
    }

  // Function for retrieving all url data from db

  const savedUrl = () => {
    Axios.get("http://localhost:3001/all").then((response) => {
        geturl(response.data); //Getting all url Database saved in database
    })
  }

  // Copying the event.target.value input contnt

  const copyUrl = () => {
    navigator.clipboard.writeText(url);
    setState(!state);
  }

  //deleteList it as temporary delete from jsonData
  const deleteList = (index) =>{
    var newList = listUrl; 
    newList.splice(index,1); //arrayName.splice([ItemLocation, [number of items]]) 
    geturl([...newList]);

    //Getting all url Database [Show All Url]

    //Axios.delete("http://localhost:3001/all").then(() =>  {
    //console.log("Delete Successfully")
  //})              
    
  } 

  // return Statement
  return (

      <div className="container  <ParticlesBackground />"> 
        <nav className='Navbar'>
                <GiButterfly className='icons' size= "3rem"/>
                <h1> Intrado project</h1>
            </nav>
            <div className="inputcontainer">
            <input className="styleChange"
            onChange={(e) => {seturl(e.target.value)}} type="text" value={ url } 
            placeholder="Enter any url" 
            required 
            />
            <button className='btn' type='button' onClick={submiturl}> <VscSearch /></button>
           </div>
          <br />
         <button className={state ? "copy":"copied" } onClick={ copyUrl }> {state ? "copied":"copy"} </button> 
        <br />
        <div>
          <AiOutlineArrowRight />
        <button className="btn3" onClick={ savedUrl }>Avaiable link</button>
        <AiOutlineArrowLeft />
        </div>
      <br />
        <table className="table   table-striped">
            <thead>
              <tr>
                <th scope="col">Short Url</th>
                <th scope="col">Unique id</th>
                 <th scope="col">delete</th>
              </tr>
            </thead>
      {listUrl.map((urlValue) => {
        return(
            <tbody>
              <tr>
                <td>{ urlValue.fullurl }</td>
                <th scope="row">{ urlValue.shirnkUrl }</th>
                <button type="button" onClick={ deleteList }><FiDelete /></button>
              </tr>
            </tbody>
        )
      })}
        </table> 
        
      </div>
  );
}

//above jsx making list what you saved previously on Database and structure of user profile

export default App;
