# URL Shortner

## Features
- Shrink  the URL in short form
- Database Storage

## Tech Stack :
- Database: Mysql 8.x, 
- Backend: NodeJS , ExpressJS
- Frontend: React Js
- Libraries : Axios , ShortId, Cors

## Guidance to run the program

Install the dependencies and devDependencies and start the server.
 Open terminal and run the  `npm install`  for both [user] and [server]

After Completion Start the both server

Open Terminal change directory to [user] and run the  `npm start` 

Add split terminal on change directory to [server] and run [`node index.js`] 
On a server directory Using a nodemon on purpose of based applications by automatically restarting the node application when file changes in the directory are detected . To run the terminal run `npm run devStart`

## Database

The `configuration` contains a list of named database connections each with the following entries
host - the hostname or ip address to connect to
port - the port mysql is listening (default: 3306)
database - the name of the database to use
user - the name used to log into the database
password - the password to login
charset - the charset used if no other given
timezone - the timezone to use
connectTimeout - the time till a connection should be established
connectionLimit - the maximum number of parallel used connections

Usage:
First you have to include the module:

Mysql = require('alinex-mysql');
The complete setup for the database connection is done smoothly in the background using the alinex-config module with the mysql.yml file as default. If you have it in another file call:

Mysql.init(config);
The config here may be a name or the configuration structure.

After that you setup the database by giving an database alias which is set in the configuration:

db = Mysql.instance('test');

Open ``` database``` folder and import ```sqldump``` file to your workbench
Mysql Database Details :
- Database Name: `url`
- Table Name: `urltable`
- Columns:
-- `id [INT][PK][AI]`
-- `fullurl [VARCHAR]`
-- `shrinkurl [VARCHAR]`

Open ` server` folder then open `index.js` and update your database credentials

`
const db = mysql.createConnection({
    user: 'root',
    host: 'localhost',
    password: 'password',
    database: 'url'
}}
`

## Server 
- Frontend: `https://localhost:3000`
- Backend: `https://localhost:3001`



